package br.granbery.agenciaBancaria.Principal;

import java.util.Scanner;

import br.granbery.agenciaBancaria.Controller.*;
import br.granbery.agenciaBancaria.Model.Cliente;
import br.granbery.agenciaBancaria.Model.Conta;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Conta conta = new Conta();
		Cliente cliente = new Cliente();
		
		menuPrincipal(conta, cliente);

	}
	
	public static void menuPrincipal (Conta conta)
	{
		Scanner s = new Scanner(System.in);
		System.out.println(" ### ***** SISTEMA DA AG�NCIA BANC�RIA ***** ### \r\n");
		System.out.println(" ### M E N U     P R I N C I P A L ### \r\n");
		
		System.out.println("1 - Autenticar: ");
		System.out.println("2 - Sair: ");
		System.out.println("Digite a sua op��o: ");
		String opcao = s.nextLine();
		
		switch (opcao) {
		case "1":
			System.out.println(" \r\n Digite a ag�ncia de sua conta: \r\n");
			String nomeAgencia = s.nextLine();
			System.out.println(" \r\n Digite a sua conta: \r\n");
			String nomeConta = s.nextLine();
			System.out.println(" \r\n Digite a sua senha: \r\n");
			String nomeSenha = s.nextLine();
			System.out.println(" \r\n Digite o seu CPF: \r\n");
			String nomeCpf = s.nextLine();
			
			conta.setAgenciaConta(Integer.parseInt(nomeAgencia));
			conta.setNumeroConta(Integer.parseInt(nomeConta));
			
			cliente.setSenhaAcesso(nomeSenha);
			cliente.setCpf(nomeCpf);
			cliente.setConta(conta);
			AuntenticacaoUsuario au = new AuntenticacaoUsuario();
			au.verificarUsuario(conta, cliente);
			/*if ((au.verificarUsuario(agencia, conta, senha, cpf)) == true){
				menuSecundario(conta);
			} else {
				System.out.println("N�O FOI POSS�VEL ENCONTRAR ESTA CONTA, TENTE NOVAMENTE!!! \r\n");
				menuPrincipal();
			}			
			break;
			*/
		case "2":
			System.exit(0);
			break;
		default:
			break;
		}
		
	}
	
	
	public static void menuSecundario (Conta conta)
	{
		Scanner t = new Scanner(System.in);
		Visualizacao visu = new Visualizacao();
		Operacao oper = new Operacao();
		
		System.out.println(" ### ***** SISTEMA DA AG�NCIA BANC�RIA ***** ### \r\n");
		System.out.println(" ### M E N U     S E C U N D � R I O ### \r\n");
		
		System.out.println("1 - Voltar ao Menu Principal: ");
		System.out.println("2 - Visualizar Saldo: ");
		System.out.println("3 - Visualizar Cadastro: ");
		System.out.println("4 - Visualizar Extrato: ");
		System.out.println("5 - Realizar Saque: ");
		System.out.println("6 - Realizar Dep�sito: ");
		System.out.println("7 - Realizar Transfer�ncia: ");
		System.out.println("8 - Realizar Pagamento: ");
		System.out.println("9 - Realizar Investimento: ");
		
		System.out.println("Digite a sua op��o: \r\n");
		String opcao = t.nextLine();
		
		switch (opcao) {
		case "1":
			menuPrincipal(conta);
			break;
		case "2":
			System.out.println(visu.visualizarSaldo(conta));
			break;
		case "3":
			System.out.println(visu.visualizarCadastro(conta));
			break;
		case "4":
			System.out.println(visu.visualizarExtrato(conta));
			break;
		case "5":
			System.out.println("O seu saldo atual � de: " + conta.getSaldo());
			System.out.println("Informe qual o valor do saque que ser� realizado: ");
			String valorSaque = t.nextLine();
			
			System.out.println(oper.realizarSaque(conta, Double.parseDouble(valorSaque)));			
			break;
		case "6":
			System.out.println("Informe qual o valor do dep�sito que ser� realizado: ");
			String valorDeposito = t.nextLine();
			
			System.out.println(oper.realizarDeposito(conta, Double.parseDouble(valorDeposito)));	
			break;
		case "7":
			System.out.println("Informe a numera��o do c�digo de barras do boleto a ser pago: ");
			String agenciaDestino = t.nextLine();
			System.out.println("Informe qual o valor do investimento que ser� realizado: ");
			String contaDestino = t.nextLine();
			System.out.println("Informe qual o valor do investimento que ser� realizado: ");
			String valorTransferencia = t.nextLine();
			Conta conta2 = new Conta();
			conta2.setAgenciaConta(Integer.parseInt(agenciaDestino));
			conta2.setNumeroConta(Integer.parseInt(contaDestino));			
			
			System.out.println(oper.realizarTransferencia(conta, conta2, Double.parseDouble(valorTransferencia)));
			break;
		case "8":
			System.out.println("Informe a numera��o do c�digo de barras do boleto a ser pago: ");
			String codigoBoleto = t.nextLine();
			System.out.println("Informe qual o valor do investimento que ser� realizado: ");
			String valorPagamento = t.nextLine();
			
			System.out.println(oper.realizarPagamento(conta, Double.parseDouble(valorPagamento), codigoBoleto));
			break;
		case "9":
			System.out.println("Informe qual o valor do investimento que ser� realizado: ");
			String valorInvestimento = t.nextLine();
			
			System.out.println(oper.realizarInvestimento(conta, Double.parseDouble(valorInvestimento)));
			break;
		default:
			System.out.println("Op��o Inv�lida, tente novamente!!! ");
			break;
		}
		
	}

}
