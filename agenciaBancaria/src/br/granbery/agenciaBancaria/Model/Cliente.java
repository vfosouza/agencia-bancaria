package br.granbery.agenciaBancaria.Model;

import java.util.Date;

public class Cliente {
	private String nome;
	private String cpf;
	private Date dataNascimento;
	private Date dataCadastro;
	private double renda;
	private String senhaAcesso;
	private String telefoneResidencial;
	private String telefoneCelular;
	private Endereco endereco;
	private Conta conta;
	
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public Date getDataCadastro() {
		return dataCadastro;
	}
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	public double getRenda() {
		return renda;
	}
	public void setRenda(double renda) {
		this.renda = renda;
	}
	public String getSenhaAcesso() {
		return senhaAcesso;
	}
	public void setSenhaAcesso(String senhaAcesso) {
		this.senhaAcesso = senhaAcesso;
	}
	public String getTelefoneResidencial() {
		return telefoneResidencial;
	}
	public void setTelefoneResidencial(String telefoneResidencial) {
		this.telefoneResidencial = telefoneResidencial;
	}
	public String getTelefoneCelular() {
		return telefoneCelular;
	}
	public void setTelefoneCelular(String telefoneCelular) {
		this.telefoneCelular = telefoneCelular;
	}
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	public Conta getConta() {
		return conta;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public boolean autentica (Conta conta)
	{
		if (conta.equals(this.getConta()))
		{
			return true;
		} 
		else 
		{
			return false;
		}
	}
	
	public String toString( Conta conta) {
		String mensagem = null;
		for (Cliente lista : conta.getCliente()) {
			mensagem = "Nome Cliente: " + lista.getNome().toString() + "\r\n"
		             + "CPF Cliente: " + lista.getCpf().toString() + "\r\n"
					 + "Nro Conta: " + lista.getConta().toString() + "\r\n"
					 + "Data de Nascimento: " + lista.getDataNascimento().toString() + "\r\n" 
					 + "Renda Cliente: " + lista.getRenda() + "\r\n"
					 + "Telefone Residencial: " + lista.getTelefoneResidencial().toString() + "\r\n"
					 + "Telefone Celular: " + lista.getTelefoneCelular().toString() + "\r\n"
					 + "Senha de Acesso: " + lista.getSenhaAcesso().toString() + "\r\n"
					 + "Rua: " + lista.getEndereco().getNomeRua().toString() + "\r\n"
					 + "Nro Residencial: " + lista.getEndereco().getNumeroResidencia() + "\r\n"
					 + "Bairro: " + lista.getEndereco().getBairro().toString() + "\r\n"
					 + "CEP: " + lista.getEndereco().getCep().toString() + "\r\n"
					 + "Cidade: " + lista.getEndereco().getCidade().toString() + "\r\n"
					 + "Estado: " + lista.getEndereco().getEstado().toString() + "\r\n"
					 + "Data de Cadastro: " + lista.getDataCadastro().toString();
		}
		return mensagem;
	}
}
