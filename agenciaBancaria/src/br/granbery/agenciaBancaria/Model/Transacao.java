package br.granbery.agenciaBancaria.Model;

import java.util.Date;

public class Transacao {
	private Conta conta;
	private Date dataTransacao;
	private String descricao;

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public Date getDataTransacao() {
		return dataTransacao;
	}

	public void setDataTransacao(Date dataTransacao) {
		this.dataTransacao = dataTransacao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String toString( Conta conta) {
		String mensagem = null;
		for (Transacao lista : conta.getExtrato()) {
			mensagem = "Nro Conta: " + lista.getConta().getNumeroConta()
					+ "\r\n" + "Agc Conta: " 
					+ lista.getConta().getAgenciaConta() + "\r\n"
					+ "Tipo Conta: "
					+ lista.getConta().getTipoConta().toString() + "\r\n"
					+ "Cliente: " + lista.getConta().getCliente().toString()
					+ "\r\nn" + "Data Transa��o: "
					+ lista.getDataTransacao().toString() + "\r\n"
					+ "Descricao: " + lista.getDescricao().toString();
		}
		return mensagem;
	}
}
