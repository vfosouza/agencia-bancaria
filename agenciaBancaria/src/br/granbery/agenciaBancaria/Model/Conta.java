package br.granbery.agenciaBancaria.Model;

import java.util.List;

import br.granbery.agenciaBancaria.DAO.FormatoConta;
import br.granbery.agenciaBancaria.DAO.TipoConta;

public class Conta {
	private int numeroConta;
	private int agenciaConta;
	private List<Cliente> cliente;
	private Enum<TipoConta> tipoConta;
	private Enum<FormatoConta> formatoConta;
	private double saldo;
	private double limiteChequeEspecial;
	private List<Transacao> extrato;
	private double movimentoDiario;
	
	
	public int getNumeroConta() {
		return numeroConta;
	}
	public void setNumeroConta(int numeroConta) {
		this.numeroConta = numeroConta;
	}
	public int getAgenciaConta() {
		return agenciaConta;
	}
	public void setAgenciaConta(int agenciaConta) {
		this.agenciaConta = agenciaConta;
	}
	public List<Cliente> getCliente() {
		return cliente;
	}
	public void setCliente(List<Cliente> cliente) {
		this.cliente = cliente;
	}
	public Enum<TipoConta> getTipoConta() {
		return tipoConta;
	}
	public void setTipoConta(Enum<TipoConta> tipoConta) {
		this.tipoConta = tipoConta;
	}
	public Enum<FormatoConta> getFormatoConta() {
		return formatoConta;
	}
	public void setFormatoConta(Enum<FormatoConta> formatoConta) {
		this.formatoConta = formatoConta;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public double getLimiteChequeEspecial() {
		return limiteChequeEspecial;
	}
	public void setLimiteChequeEspecial(double limite) {
		this.limiteChequeEspecial = limite;
	}
	public List<Transacao> getExtrato() {
		return extrato;
	}
	public void setExtrato(List<Transacao> extrato) {
		this.extrato = extrato;
	}
	public double getMovimentoDiario() {
		return movimentoDiario;
	}
	public void setMovimentoDiario(double movimentoDiario) {
		this.movimentoDiario = movimentoDiario;
	}
}
