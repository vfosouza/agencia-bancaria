package br.granbery.agenciaBancaria.Controller;

import br.granbery.agenciaBancaria.DAO.ContaExecutiva;
import br.granbery.agenciaBancaria.DAO.ContaPremium;
import br.granbery.agenciaBancaria.DAO.ContaSalario;
import br.granbery.agenciaBancaria.DAO.FormatoConta;
import br.granbery.agenciaBancaria.Model.Cliente;
import br.granbery.agenciaBancaria.Model.Conta;

public class Visualizacao {
	Cliente cliente = new Cliente();
	
	public String visualizarSaldo (Conta conta){
		String mensagem = null;
		if (conta.getFormatoConta().equals(FormatoConta.SALARIO)){
			ContaSalario cs = new ContaSalario();
			mensagem = cs.visualizarSaldo(conta);
		} 
		else if (conta.getFormatoConta().equals(FormatoConta.EXECUTIVA)){
			ContaExecutiva ce = new ContaExecutiva();
			mensagem = ce.visualizarSaldo(conta);
		}
		else if (conta.getFormatoConta().equals(FormatoConta.PREMIUM)){
			ContaPremium cp = new ContaPremium();
			mensagem = cp.visualizarSaldo(conta);
		}
		return mensagem;
	}
	
	public String visualizarCadastro (Conta conta){
		String mensagem = null;
		if (conta.getFormatoConta().equals(FormatoConta.SALARIO)){
			ContaSalario cs = new ContaSalario();
			mensagem = cs.visualizaCadastro(conta);
		} 
		else if (conta.getFormatoConta().equals(FormatoConta.EXECUTIVA)){
			ContaExecutiva ce = new ContaExecutiva();
			mensagem = ce.visualizaCadastro(conta);
		}
		else if (conta.getFormatoConta().equals(FormatoConta.PREMIUM)){
			ContaPremium cp = new ContaPremium();
			mensagem = cp.visualizaCadastro(conta);
		}
		return mensagem;
	}
	
	public String visualizarExtrato (Conta conta){
		String mensagem = null;
		if (conta.getFormatoConta().equals(FormatoConta.SALARIO)){
			ContaSalario cs = new ContaSalario();
			mensagem = cs.extrato(conta);
		} 
		else if (conta.getFormatoConta().equals(FormatoConta.EXECUTIVA)){
			ContaExecutiva ce = new ContaExecutiva();
			mensagem = ce.extrato(conta);
		}
		else if (conta.getFormatoConta().equals(FormatoConta.PREMIUM)){
			ContaPremium cp = new ContaPremium();
			mensagem = cp.extrato(conta);
		}
		return mensagem;
	}
}
