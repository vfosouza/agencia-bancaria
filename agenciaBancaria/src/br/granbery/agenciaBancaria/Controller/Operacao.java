package br.granbery.agenciaBancaria.Controller;

import br.granbery.agenciaBancaria.DAO.ContaExecutiva;
import br.granbery.agenciaBancaria.DAO.ContaPremium;
import br.granbery.agenciaBancaria.DAO.ContaSalario;
import br.granbery.agenciaBancaria.DAO.FormatoConta;
import br.granbery.agenciaBancaria.Model.Conta;

public class Operacao {
	public String realizarSaque (Conta conta, double valor) 
	{
		String mensagem = null;
		if (conta.getFormatoConta().equals(FormatoConta.SALARIO)){
			ContaSalario cs = new ContaSalario();
			mensagem = cs.saque(conta, valor);
		} 
		else if (conta.getFormatoConta().equals(FormatoConta.EXECUTIVA)){
			ContaExecutiva ce = new ContaExecutiva();
			mensagem = ce.saque(conta, valor);
		}
		else if (conta.getFormatoConta().equals(FormatoConta.PREMIUM)){
			ContaPremium cp = new ContaPremium();
			mensagem = cp.saque(conta, valor);
		}
		return mensagem;
	}
	
	public String realizarDeposito (Conta conta, double valor) 
	{
		String mensagem = null;
		if (conta.getFormatoConta().equals(FormatoConta.SALARIO)){
			ContaSalario cs = new ContaSalario();
			mensagem = cs.deposito(conta, valor);
		} 
		else if (conta.getFormatoConta().equals(FormatoConta.EXECUTIVA)){
			ContaExecutiva ce = new ContaExecutiva();
			mensagem = ce.deposito(conta, valor);
		}
		else if (conta.getFormatoConta().equals(FormatoConta.PREMIUM)){
			ContaPremium cp = new ContaPremium();
			mensagem = cp.deposito(conta, valor);
		}
		return mensagem;
	}
	
	public String realizarTransferencia (Conta conta1, Conta conta2, double valor) 
	{
		String mensagem = null;
		if (conta1.getFormatoConta().equals(FormatoConta.SALARIO)){
			ContaSalario cs = new ContaSalario();
			mensagem = cs.transferencia(conta1, conta2, valor);
		} 
		else if (conta1.getFormatoConta().equals(FormatoConta.EXECUTIVA)){
			ContaExecutiva ce = new ContaExecutiva();
			mensagem = ce.transferencia(conta1, conta2, valor);
		}
		else if (conta1.getFormatoConta().equals(FormatoConta.PREMIUM)){
			ContaPremium cp = new ContaPremium();
			mensagem = cp.transferencia(conta1, conta2, valor);
		}
		return mensagem;
	}
	
	public String realizarPagamento (Conta conta, double valor, String codigoBoleto) 
	{
		String mensagem = null;
		if (conta.getFormatoConta().equals(FormatoConta.SALARIO)){
			ContaSalario cs = new ContaSalario();
			mensagem = cs.pagamento(conta, valor, codigoBoleto);
		} 
		else if (conta.getFormatoConta().equals(FormatoConta.EXECUTIVA)){
			ContaExecutiva ce = new ContaExecutiva();
			mensagem = ce.pagamento(conta, valor, codigoBoleto);
		}
		else if (conta.getFormatoConta().equals(FormatoConta.PREMIUM)){
			ContaPremium cp = new ContaPremium();
			mensagem = cp.pagamento(conta, valor, codigoBoleto);
		}
		return mensagem;
	}
	
	public String realizarInvestimento (Conta conta, double valor) 
	{
		String mensagem = null;
		if (conta.getFormatoConta().equals(FormatoConta.SALARIO)){
			ContaSalario cs = new ContaSalario();
			mensagem = cs.investimento(conta, valor);
		} 
		else if (conta.getFormatoConta().equals(FormatoConta.EXECUTIVA)){
			ContaExecutiva ce = new ContaExecutiva();
			mensagem = ce.investimento(conta, valor);
		}
		else if (conta.getFormatoConta().equals(FormatoConta.PREMIUM)){
			ContaPremium cp = new ContaPremium();
			mensagem = cp.investimento(conta, valor);
		}
		return mensagem;
	}
}
