package br.granbery.agenciaBancaria.DAO;

public enum TipoConta {
	CONJUNTA, UNICA;
}