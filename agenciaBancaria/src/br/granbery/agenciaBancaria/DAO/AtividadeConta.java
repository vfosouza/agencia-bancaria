package br.granbery.agenciaBancaria.DAO;

import br.granbery.agenciaBancaria.Model.Conta;

public interface AtividadeConta {
	String visualizarSaldo (Conta conta);
	String saque (Conta conta, double valor);
	String deposito (Conta conta, double valor);
	String extrato (Conta conta);
	String visualizaCadastro (Conta conta);
	boolean naoNegativar (Conta conta, double valor);
	
	// Atributos para contas Executiva e Premium
	String transferencia (Conta conta1, Conta conta2, double valor);
	String pagamento (Conta conta, double valor, String boletoBancario);
	boolean movimentoMaximo (Conta conta);
	
	// Adicionar Transa��o
	void adicionarTransacao (Conta conta, String mensagem);
	
	// Adicionar Investimento
	String investimento (Conta conta, double valor);
}
