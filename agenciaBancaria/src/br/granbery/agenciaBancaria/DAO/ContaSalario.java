package br.granbery.agenciaBancaria.DAO;

import java.util.Date;
import java.util.List;

import br.granbery.agenciaBancaria.Model.Cliente;
import br.granbery.agenciaBancaria.Model.Conta;
import br.granbery.agenciaBancaria.Model.Transacao;

public class ContaSalario extends Conta implements AtividadeConta{
	private Cliente cliente = new Cliente();	
	private String mensagem = null;
	private List<Transacao> lista;
	
	@Override
	public String saque(Conta conta, double valor) {
		// TODO Auto-generated method stub
		if (cliente.autentica(conta))
		{
			try {
				double limite = 0.0f;
				double custo = 0.0f;
				double saldo = ((conta.getSaldo() - valor) - custo);
				
				boolean negativar = this.naoNegativar(conta, (valor * -(1)));
				if (negativar == true)
				{
					if ((saldo >= 0) && ((saldo - limite) >= 0))
					{
						conta.setSaldo(saldo);
						conta.setMovimentoDiario(conta.getMovimentoDiario() + (valor + custo));
						mensagem = "SAQUE REALIZADO NO VALOR DE: " + valor + "! \r\n" +
						           "SEU SALDO ATUAL � NO VALOR DE: " + conta.getSaldo();
						adicionarTransacao(conta, mensagem);
					} 
					else
					{
						mensagem = "VOC� N�O POSSUI SALDO SUFICIENTE PARA REALIZAR ESTE SAQUE! ";
						adicionarTransacao(conta, mensagem);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else
		{
			mensagem = "SUAS CREDENCIAIS N�O CONFEREM COM AS CADASTRADAS NO SISTEMA, TENTE NOVAMENTE! ";
			adicionarTransacao(conta, mensagem);
		}
		return mensagem;
	}

	@Override
	public String deposito(Conta conta, double valor) {
		// TODO Auto-generated method stub
		if (cliente.autentica(conta))
		{
			try {
				double limite = 0.0f;
				double custo = 0.0f;
				double saldo = ((conta.getSaldo() + valor) - custo);
				
				boolean negativar = this.naoNegativar(conta, valor);
				if (negativar == true)
				{
					if ((saldo >= 0) && ((saldo - limite) >= 0))
					{
						conta.setSaldo(saldo);
						conta.setMovimentoDiario(conta.getMovimentoDiario() + (valor + custo));
						mensagem = "DEPOSITO REALIZADO NO VALOR DE: " + valor + "! \r\n" +
						           "SEU SALDO ATUAL � NO VALOR DE: " + conta.getSaldo();
						adicionarTransacao(conta, mensagem);
					} 
					else
					{
						mensagem = "VOC� N�O POSSUI SALDO SUFICIENTE PARA REALIZAR ESTE DEPOSITO! ";
						adicionarTransacao(conta, mensagem);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else
		{
			mensagem = "SUAS CREDENCIAIS N�O CONFEREM COM AS CADASTRADAS NO SISTEMA, TENTE NOVAMENTE! ";
			adicionarTransacao(conta, mensagem);
		}
		return mensagem;
	}

	@Override
	public String extrato(Conta conta) {
		// TODO Auto-generated method stub
		if (cliente.autentica(conta))
		{
			Transacao t = new Transacao();
			mensagem = t.toString(conta);
			adicionarTransacao(conta, mensagem);
		}
		else
		{
			mensagem = "SUAS CREDENCIAIS N�O CONFEREM COM AS CADASTRADAS NO SISTEMA, TENTE NOVAMENTE! ";
			adicionarTransacao(conta, mensagem);
		}
		return mensagem;
	}

	@Override
	public String visualizaCadastro(Conta conta) {
		// TODO Auto-generated method stub
		if (cliente.autentica(conta))
		{
			Cliente c = new Cliente();
			mensagem = c.toString(conta);
			adicionarTransacao(conta, ("VISUALIZA��O DE CADASTRO: " + mensagem));
		}
		else
		{
			mensagem = "SUAS CREDENCIAIS N�O CONFEREM COM AS CADASTRADAS NO SISTEMA, TENTE NOVAMENTE! ";
			adicionarTransacao(conta, mensagem);
		}
		return null;
	}

	@Override
	public boolean naoNegativar(Conta conta, double valor) {
		// TODO Auto-generated method stub
		if (cliente.autentica(conta))
		{
			if ((conta.getSaldo() + valor) < 0.0f) {
				mensagem = "A��O N�O PERMITIDA, POR RAZ�ES DE SUA CONTA SER NEGATIVADA! ";
				adicionarTransacao(conta, mensagem);
				return false;
			} 
		}
		else
		{
			mensagem = "SUAS CREDENCIAIS N�O CONFEREM COM AS CADASTRADAS NO SISTEMA, TENTE NOVAMENTE! ";
			adicionarTransacao(conta, mensagem);
		}
		return true;
	}

	@Override
	public String transferencia(Conta conta1, Conta conta2, double valor) {
		// TODO Auto-generated method stub
		if (conta1.getFormatoConta().equals(FormatoConta.SALARIO))
		{
			mensagem = "O SEU TIPO DE CONTA N�O PERMITE ESTA OP��O! ";
			adicionarTransacao(conta1, mensagem);
		}
		return mensagem;
	}

	@Override
	public String pagamento(Conta conta, double valor, String boletoBancario) {
		// TODO Auto-generated method stub
		if (conta.getFormatoConta().equals(FormatoConta.SALARIO))
		{
			mensagem = "O SEU TIPO DE CONTA N�O PERMITE ESTA OP��O! ";
			adicionarTransacao(conta, mensagem);
		}
		return mensagem;
	}

	@Override
	public boolean movimentoMaximo(Conta conta) {
		// TODO Auto-generated method stub
		if (conta.getFormatoConta().equals(FormatoConta.SALARIO))
		{
			mensagem = "O SEU TIPO DE CONTA N�O PERMITE ESTA OP��O! ";
			adicionarTransacao(conta, mensagem);
		}
		return false;
	}

	@Override
	public void adicionarTransacao(Conta conta, String mensagem) {
		lista = null;
		try {
			Transacao t = new Transacao();
			t.setConta(conta);
			t.setDataTransacao(new Date(System.currentTimeMillis()));
			t.setDescricao(mensagem);
			lista.add(t);
			conta.setExtrato(lista);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String investimento(Conta conta, double valor) {
		// TODO Auto-generated method stub
		if (conta.getFormatoConta().equals(FormatoConta.SALARIO))
		{
			mensagem = "O SEU TIPO DE CONTA N�O PERMITE ESTA OP��O! ";
			adicionarTransacao(conta, mensagem);
		}
		return mensagem;
	}

	@Override
	public String visualizarSaldo(Conta conta) {
		// TODO Auto-generated method stub
		mensagem = "O SEU SALDO � DE: " + conta.getSaldo() + "! ";
		adicionarTransacao(conta, mensagem);
		return mensagem;
	}
}
