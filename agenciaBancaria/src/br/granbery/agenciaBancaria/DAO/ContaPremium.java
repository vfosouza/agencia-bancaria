package br.granbery.agenciaBancaria.DAO;

import java.util.Date;
import java.util.List;

import br.granbery.agenciaBancaria.Model.Cliente;
import br.granbery.agenciaBancaria.Model.Conta;
import br.granbery.agenciaBancaria.Model.Transacao;

public class ContaPremium extends Conta implements AtividadeConta{
	private Cliente cliente = new Cliente();	
	private String mensagem = null;
	private List<Transacao> lista;
	
	@Override
	public String saque(Conta conta, double valor) {
		// TODO Auto-generated method stub
		if (cliente.autentica(conta))
		{
			try {
				double custo = this.calculaCusto("Saque");
				double saldo = ((conta.getSaldo() - valor) - custo);
				
				boolean negativar = this.naoNegativar(conta, (valor * -(1)));
				if (negativar == true)
				{
					boolean movimentacao = this.movimentoMaximo(conta);
					if (movimentacao == true)
					{
						if ((saldo >= 0) && ((saldo - conta.getLimiteChequeEspecial()) >= 0))
						{
							conta.setSaldo(saldo);
							conta.setMovimentoDiario(conta.getMovimentoDiario() + (valor + custo));
							mensagem = "SAQUE REALIZADO NO VALOR DE: " + valor + "! \r\n" +
							           "SEU SALDO ATUAL � NO VALOR DE: " + conta.getSaldo();
							adicionarTransacao(conta, mensagem);
						} 
						else
						{
							mensagem = "VOC� N�O POSSUI SALDO SUFICIENTE PARA REALIZAR ESTE SAQUE! ";
							adicionarTransacao(conta, mensagem);
						}
					} 
					// Movimenta��o
					mensagem = "TRANSA��O N�O REALIZADA, POR RAZ�O DE J� TER ATINGIDO O SEU LIMITE M�XIMO DE MOVIMENTA��O DI�RIA! ";
					adicionarTransacao(conta, mensagem);
				}
				// Negativar
				mensagem = "A��O N�O PERMITIDA, POR RAZ�ES DE SUA CONTA SER NEGATIVADA! ";
				adicionarTransacao(conta, mensagem);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else
		{
			mensagem = "SUAS CREDENCIAIS N�O CONFEREM COM AS CADASTRADAS NO SISTEMA, TENTE NOVAMENTE! ";
			adicionarTransacao(conta, mensagem);
		}
		return mensagem;
	}

	@Override
	public String deposito(Conta conta, double valor) {
		// TODO Auto-generated method stub
		if (cliente.autentica(conta))
		{
			try {
				double custo = this.calculaCusto("Deposito");
				double saldo = ((conta.getSaldo() + valor) - custo);
				
				boolean negativar = this.naoNegativar(conta, valor);
				if (negativar == true)
				{
					boolean movimentar = this.movimentoMaximo(conta);
					if (movimentar == true)
					{
						if ((saldo >= 0) && ((saldo - conta.getLimiteChequeEspecial()) >= 0))
						{
							conta.setSaldo(saldo);
							conta.setMovimentoDiario(conta.getMovimentoDiario() + (valor + custo));
							mensagem = "DEPOSITO REALIZADO NO VALOR DE: " + valor + "! \r\n" +
							           "SEU SALDO ATUAL � NO VALOR DE: " + conta.getSaldo();
							adicionarTransacao(conta, mensagem);
						} 
						else
						{
							mensagem = "VOC� N�O POSSUI SALDO SUFICIENTE PARA REALIZAR ESTE DEPOSITO! ";
							adicionarTransacao(conta, mensagem);
						}
					} 
					// Movimenta��o
					mensagem = "TRANSA��O N�O REALIZADA, POR RAZ�O DE J� TER ATINGIDO O SEU LIMITE M�XIMO DE MOVIMENTA��O DI�RIA! ";
					adicionarTransacao(conta, mensagem);
				}
				// Negativar
				mensagem = "A��O N�O PERMITIDA, POR RAZ�ES DE SUA CONTA SER NEGATIVADA! ";
				adicionarTransacao(conta, mensagem);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else
		{
			mensagem = "SUAS CREDENCIAIS N�O CONFEREM COM AS CADASTRADAS NO SISTEMA, TENTE NOVAMENTE! ";
			adicionarTransacao(conta, mensagem);
		}
		return mensagem;
	}

	@Override
	public String extrato(Conta conta) {
		// TODO Auto-generated method stub
		if (cliente.autentica(conta))
		{
			Transacao t = new Transacao();
			mensagem = t.toString(conta);
			adicionarTransacao(conta, mensagem);
		}
		else
		{
			mensagem = "SUAS CREDENCIAIS N�O CONFEREM COM AS CADASTRADAS NO SISTEMA, TENTE NOVAMENTE! ";
			adicionarTransacao(conta, mensagem);
		}
		return mensagem;
	}

	@Override
	public String visualizaCadastro(Conta conta) {
		// TODO Auto-generated method stub
		if (cliente.autentica(conta))
		{
			Cliente c = new Cliente();
			mensagem = c.toString(conta);
			adicionarTransacao(conta, ("VISUALIZA��O DE CADASTRO: " + mensagem));
		}
		else
		{
			mensagem = "SUAS CREDENCIAIS N�O CONFEREM COM AS CADASTRADAS NO SISTEMA, TENTE NOVAMENTE! ";
			adicionarTransacao(conta, mensagem);
		}
		return null;
	}

	@Override
	public boolean naoNegativar(Conta conta, double valor) {
		// TODO Auto-generated method stub
		if (cliente.autentica(conta))
		{
			if ((conta.getSaldo() + conta.getLimiteChequeEspecial() + valor) < 0.0f) {
				mensagem = "A��O N�O PERMITIDA, POR RAZ�ES DE SUA CONTA SER NEGATIVADA! ";
				adicionarTransacao(conta, mensagem);
				return false;
			} 
		}
		else
		{
			mensagem = "SUAS CREDENCIAIS N�O CONFEREM COM AS CADASTRADAS NO SISTEMA, TENTE NOVAMENTE! ";
			adicionarTransacao(conta, mensagem);
		}
		return true;
	}
	

	@Override
	public String transferencia(Conta conta1, Conta conta2, double valor) {
		// TODO Auto-generated method stub
		double custo = this.calculaCusto("Transferencia");
		if (cliente.autentica(conta1))
		{
			try {
				boolean negativar = this.naoNegativar(conta1, valor);
				if (negativar == true)
				{
					boolean movimentar = this.movimentoMaximo(conta1);
					if (movimentar == true)
					{
						saque(conta1, (valor + custo));	
						conta1.setMovimentoDiario(conta1.getMovimentoDiario() +(valor + custo));
						deposito(conta2, valor);
						mensagem = "TRANSFERENCIA REALIZADA NO VALOR DE: " + valor + "! \r\n" +
						           "Agencia Origem: " + conta1.getAgenciaConta() + " CTA Origem: " + conta1.getNumeroConta() + "\r\n" +
						           "Agencia Destino: " + conta2.getAgenciaConta() + " CTA Destino: " + conta2.getNumeroConta() + "\r\n" +
						           "O CUSTO PARA A REALIZA��O DA TRANSFERENCIA FOI DE: " + custo + "\r\n" +
							       "SEU SALDO ATUAL � NO VALOR DE: " + conta1.getSaldo();
						adicionarTransacao(conta1, mensagem);
					} 
					// Movimenta��o
					mensagem = "TRANSA��O N�O REALIZADA, POR RAZ�O DE J� TER ATINGIDO O SEU LIMITE M�XIMO DE MOVIMENTA��O DI�RIA! ";
					adicionarTransacao(conta1, mensagem);
				}
				// Negativar
				mensagem = "A��O N�O PERMITIDA, POR RAZ�ES DE SUA CONTA SER NEGATIVADA! ";
				adicionarTransacao(conta1, mensagem);
			} catch (Exception e) 
			{
				e.printStackTrace();
				mensagem = "ERRO AO REALIZAR A TRANSFERENCIA! ";
			}
		}
		else
		{
			mensagem = "SUAS CREDENCIAIS N�O CONFEREM COM AS CADASTRADAS NO SISTEMA, TENTE NOVAMENTE! ";
			adicionarTransacao(conta1, mensagem);
		}
		return mensagem;
	}

	@Override
	public String pagamento(Conta conta, double valor, String boletoBancario) {
		// TODO Auto-generated method stub
		double custo = this.calculaCusto("Pagamento");
		if (cliente.autentica(conta))
		{
			try {
				boolean negativar = this.naoNegativar(conta, valor);
				if (negativar == true)
				{
					boolean movimentar = this.movimentoMaximo(conta);
					if (movimentar == true)
					{
						this.saque(conta, (valor - custo));
						conta.setMovimentoDiario(conta.getMovimentoDiario() + (valor + custo));
						mensagem = "PAGAMENTO REALIZAD0 NO VALOR DE: " + valor + "! \r\n" +
						           "Codigo do Boleto Banc�rio: " + boletoBancario.toString() + "\r\n" +
						           "O CUSTO PARA A REALIZA��O DA TRANSFERENCIA FOI DE: " + custo + "\r\n" +
							       "SEU SALDO ATUAL � NO VALOR DE: " + conta.getSaldo();
						adicionarTransacao(conta, mensagem);
					} 
					// Movimenta��o
					mensagem = "TRANSA��O N�O REALIZADA, POR RAZ�O DE J� TER ATINGIDO O SEU LIMITE M�XIMO DE MOVIMENTA��O DI�RIA! ";
					adicionarTransacao(conta, mensagem);
				}
				// Negativar
				mensagem = "A��O N�O PERMITIDA, POR RAZ�ES DE SUA CONTA SER NEGATIVADA! ";
				adicionarTransacao(conta, mensagem);
			} catch (Exception e) 
			{
				e.printStackTrace();
				mensagem = "ERRO AO REALIZAR O PAGAMENTO! ";
			}
		}
		else
		{
			mensagem = "SUAS CREDENCIAIS N�O CONFEREM COM AS CADASTRADAS NO SISTEMA, TENTE NOVAMENTE! ";
			adicionarTransacao(conta, mensagem);
		}
		return mensagem;
	}

	@Override
	public boolean movimentoMaximo(Conta conta) {
		// TODO Auto-generated method stub
		if (conta.getMovimentoDiario() >= 100000)
		{
			return false;
		}
		return true;
	}

	public void adicionarTransacao(Conta conta, String mensagem) {
		// TODO Auto-generated method stub
		lista = null;
		try {
			Transacao t = new Transacao();
			t.setConta(conta);
			t.setDataTransacao(new Date(System.currentTimeMillis()));
			t.setDescricao(mensagem);
			lista.add(t);
			conta.setExtrato(lista);
		} catch (Exception e) {
			e.printStackTrace();
			mensagem = "ERRO AO ADICIONAR TRANSA��O! ";
		}
	}

	@Override
	public String investimento(Conta conta, double valor) {
		// TODO Auto-generated method stub
		double custo = (valor * this.calculaCusto("Investimento"));
		try {
			boolean negativar = this.naoNegativar(conta, valor);
			if (negativar == true)
			{
				boolean movimentar = this.movimentoMaximo(conta);
				if (movimentar == true)
				{
					this.saque(conta, (valor + custo));
					conta.setMovimentoDiario(conta.getMovimentoDiario() + (valor + custo));
					mensagem = "INVESTIMENTO REALIZAD0 NO VALOR DE: " + valor + "! \r\n" +
					           "O CUSTO PARA A REALIZA��O DESTE INVESTIMENTO FOI DE: " + custo + "\r\n" +
						       "SEU SALDO ATUAL � NO VALOR DE: " + conta.getSaldo();
					adicionarTransacao(conta, mensagem);
		} 
		// Movimenta��o
		mensagem = "TRANSA��O N�O REALIZADA, POR RAZ�O DE J� TER ATINGIDO O SEU LIMITE M�XIMO DE MOVIMENTA��O DI�RIA! ";
		adicionarTransacao(conta, mensagem);
	}
	// Negativar
	mensagem = "A��O N�O PERMITIDA, POR RAZ�ES DE SUA CONTA SER NEGATIVADA! ";
	adicionarTransacao(conta, mensagem);
		} catch (Exception e) {
			e.printStackTrace();
			mensagem = "ERRO AO REALIZAR O INVESTIMENTO! ";
		}
		return mensagem;
	}
	
	@Override
	public String visualizarSaldo(Conta conta) {
		// TODO Auto-generated method stub
		mensagem = "O SEU SALDO � DE: " + conta.getSaldo() + "! ";
		adicionarTransacao(conta, mensagem);
		return mensagem;
	}
	
	public double calculaCusto (String operacao)
	{
		double custo = 0.0f;
		switch (operacao) {
		case "Saque":
			custo = 0.0f;
			break;
		case "Deposito":
			custo = 0.0f;
			break;
		case "Transferencia":
			custo = 4.0f;
			break;
		case "Pagamento":
			custo = 0.0f;
			break;
		case "Investimento":
			custo = 0.01f;
			break;
		default:
			break;
		}		
		return custo;
	}
}
